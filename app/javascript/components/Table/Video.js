import React from 'react'

const Video = () => {
  return (
    <div>
      <iframe width="100%" height="100%" src="https://www.youtube.com/embed/d3y7cWzOXWM" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
    </div>
  )
}

export default Video
