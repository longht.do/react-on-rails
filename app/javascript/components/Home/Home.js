import React, { useState } from 'react';
import Jumbotron from './Jumbotron';
import Table from '../Table/Table';
import axios from 'axios';

const Home = () => {
  const course_module = [
    {
      id: 1,
      title: '1. Setting up a new Rby on Rails App with React.',
      description: 'jsdfl;asfk',
      active: false
    },
    {
      id: 2,
      title: '2 Adding React to an Existing Rails App.',
      description: 'zxncvmzxcv',
      active: false
    },
    {
      id: 3,
      title: '3. Building a Hello World App.',
      description: 'poiuoiu',
      active: false
    },
    {
      id: 4,
      title: '4. Adding React Router Dom to your App.',
      description: 'pwepwe',
      active: false
    }
  ]

  const [list, setList] = useState(course_module)

  const handleVideoChange = (item, e) => {
    e.preventDefault()
    list.map(data => {
      data.active = false
    })

    item.active = !item.active

    list[item.id -1] = item

    setList(course_module)
  }

  return (
    <div>
      <Jumbotron />
      <Table handleVideoChange={handleVideoChange} list={list} />
    </div>
  )
}

export default Home
